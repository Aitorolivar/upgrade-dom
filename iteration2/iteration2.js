// 2.1 Inserta dinamicamente en un html un div vacio con javascript.

const $$divClean = document.createElement("div");
document.body.appendChild($$divClean);


// 2.2 Inserta dinamicamente en un html un div que contenga una p con javascript.

const $$div = document.createElement("div");
const $$paragrahp = document.createElement("p");
$$div.appendChild($$paragrahp);
document.body.appendChild($$div);


//2.3 Inserta dinamicamente en un html un div que contenga 6 p utilizando un loop con javascript.

const $$divP = document.createElement("div");
document.body.appendChild($$divP);
for (let i = 0; i < 6; i++) {
    const $$paragrahp1 = document.createElement("p");
    $$divP.appendChild($$paragrahp1);
};


// 2.4 Inserta dinamicamente con javascript en un html una p con el texto 'Soy dinámico!'.

const $$paragrahp2 = document.createElement("p");
$$paragrahp2.innerHTML = "Soy dinámico!";
document.body.appendChild($$paragrahp2);


// 2.5 Inserta en el h2 con la clase .fn-insert-here el texto 'Wubba Lubba dub dub'.

const $$h2 = document.querySelectorAll(".fn-insert-here");
$$h2[0].innerHTML = "Wubba Lubba dub dub";


// 2.6 Basandote en el siguiente array crea una lista ul > li con los textos del array. const apps = ['Facebook', 'Netflix', 'Instagram', 'Snapchat', 'Twitter'];

const apps = ["Facebook", "Netflix", "Instagram", "Snapchat", "Twitter"];
const $$ul = document.createElement("ul");

document.body.appendChild($$ul);
for (let app of apps) {
    const $$li = document.createElement('li');
    $$li.innerHTML = app;
    $$li.className = 'test2';//Añadir una nueva clase a cada li
    $$ul.appendChild($$li);
};


//2.7 Elimina todos los nodos que tengan la clase .fn-remove-me

const $$nodes = document.querySelectorAll(".fn-remove-me");
for (let remove of $$nodes) {
    remove.parentNode.removeChild(remove);
};


// 2.8 Inserta una p con el texto 'Voy en medio!' entre los dos div. Recuerda que no solo puedes insertar elementos con .appendChild.

function insertAfter(referenceNode, newNode) {
    referenceNode.parentNode.insertBefore(newNode, referenceNode.nextSibling);
  };
  const $$paragrahp3 = document.createElement('p');
  $$paragrahp3.innerHTML = 'Voy en medio!';
  const $$div1 = document.querySelector('div');
  insertAfter($$div1, $$paragrahp3);
 