

function init() {

// 1.1 Usa querySelector para mostrar por consola el botón con la clase .showme

  const $$button = document.body.querySelector(".showme");
  console.log($$button);


  // 1.2 Usa querySelector para mostrar por consola el h1 con el id #pillado

  const $$title = document.body.querySelector("#pillado");
  console.log($$title);


  // 1.3 Usa querySelector para mostrar por consola todos los p


  const $$paragraph = document.body.querySelectorAll("p");
  console.log($$paragraph);


  // 1.4 Usa querySelector para mostrar por consola todos los elementos con la clase.pokemon

  const $$h4 = document.body.querySelectorAll(".pokemon");
  console.log($$h4);


  // 1.5 Usa querySelector para mostrar por consola todos los elementos con el atributo data-function="testMe".

  const $$span = document.body.querySelectorAll('[data-function = "testMe"]'); //Selecciona por atributo de la etiqueta y su valor.
  console.log($$span);


  //1.6 Usa querySelector para mostrar por consola el 3 personaje con el atributo data-function="testMe".

  
  console.log($$span[2]);
}

window.onload = init;
